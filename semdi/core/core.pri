
QT += xml

HEADERS += \
    $$PWD/source/Filesystem.h \
    $$PWD/source/TextEntry.h \
    $$PWD/source/EntryRepository.h \
    $$PWD/source/FileFormat.h \
    $$PWD/source/XmlFileFormat.h \

SOURCES += \
    $$PWD/source/TextEntry.cpp \
    $$PWD/source/EntryRepository.cpp \
    $$PWD/source/XmlFileFormat.cpp \

include(../common.pri)
