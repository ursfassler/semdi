TEMPLATE = app

CONFIG -= app_bundle

QT -= gui
QT += testlib
QT += xml

HEADERS += \
    source/TextEntry.h \
    source/EntryRepository.h \
    adapter/InMemoryFilesystem.h \
    source/XmlFileFormat.h \
    source/FileFormat.h \
    source/Filesystem.h \
    ../viewmodel/feature-tests/cute-gtest.h \

SOURCES += \
    source/TextEntry.cpp \
    source/TextEntry.test.cpp \
    source/EntryRepository.cpp \
    source/EntryRepository.test.cpp \
    adapter/InMemoryFilesystem.cpp \
    source/XmlFileFormat.cpp \
    ../viewmodel/feature-tests/cute-gtest.cpp \

include(../gtest.pri)
include(../common.pri)
