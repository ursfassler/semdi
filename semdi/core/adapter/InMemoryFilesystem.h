/*
 * (C) Copyright 2018 Urs Fässler
 *
 * SPDX-License-Identifier: AGPLv3
 */

#pragma once

#include "core/source/Filesystem.h"
#include <QMap>


namespace adapter
{


class InMemoryFilesystem :
        public core::Filesystem
{
public:
    void write(const Filename&, const Content&, const WriteSuccessHandler&, const WriteErrorHandler&) override;
    void read(const Filename&, const ReadSuccessHandler&, const ReadErrorHandler&) const override;
    void list(const ListSuccessHandler&) const override;
    bool exists(const Filename&) const override;

    DirectoryList list() const;
    void setReadonly();
    void clear();
    void returnReadError();

private:
    QMap<Filename, Content> files{};
    bool readonly{false};
    bool readError{false};

};


}
