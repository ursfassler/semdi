/*
 * (C) Copyright 2018 Urs Fässler
 *
 * SPDX-License-Identifier: AGPLv3
 */

#include "InMemoryFilesystem.h"
#include <algorithm>

namespace adapter
{


void InMemoryFilesystem::write(const Filename& filename, const Content& content, const WriteSuccessHandler& success, const WriteErrorHandler& error)
{
    if (readonly) {
      error();
    } else {
        files[filename] = content;
        success();
    }
}

void InMemoryFilesystem::read(const Filename& filename, const ReadSuccessHandler& success, const ReadErrorHandler& error) const
{
  if (readError) {
    error();
    return;
  }

  if (files.contains(filename)) {
    success(files.value(filename));
  } else {
    error();
  }
}

void InMemoryFilesystem::list(const Filesystem::ListSuccessHandler& success) const
{
  const auto entries = list();
  success(entries);
}

bool InMemoryFilesystem::exists(const core::Filesystem::Filename& value) const
{
  return files.contains(value);
}

InMemoryFilesystem::DirectoryList InMemoryFilesystem::list() const
{
  return files.keys();
}

void InMemoryFilesystem::setReadonly()
{
  readonly = true;
}

void InMemoryFilesystem::clear()
{
  files.clear();
}

void InMemoryFilesystem::returnReadError()
{
  readError = true;
}


}
