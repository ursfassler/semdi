/*
 * (C) Copyright 2018 Urs Fässler
 *
 * SPDX-License-Identifier: AGPLv3
 */

#include "EntryRepository.h"

#include "Filesystem.h"
#include "FileFormat.h"


namespace core
{


EntryRepository::EntryRepository(Filesystem& filesystem_, const FileFormat& fileFormat_) :
  filesystem{filesystem_},
  fileFormat{fileFormat_}
{
}

void EntryRepository::get(const Specification& specification, const EntryRepository::GetSuccessHandler& success)
{
  //TODO do not clear entries, only load new ones
  entries.clear();

  filesystem.list([this, specification, success](const Filesystem::DirectoryList& value){
    filesToParse = value.size();
    for (const auto& filename : value) {
      loadFile(filename, specification, [this, success]{
        filesToParse--;
        if (filesToParse == 0) {
          success(entries);
        }
      });
    }
  });
}

void EntryRepository::loadFile(const Filesystem::Filename& value, const Specification& specification, const std::function<void(void)>& finished)
{
  const auto isParsable = value.endsWith(fileFormat.extension());

  if (!isParsable) {
    finished();
    return;
  }

  filesystem.read(value, [this, specification, finished](const Filesystem::Content& value){
    const auto entry = fileFormat.parse(value);
    if (specification(entry)) {
      entries.append(entry);
    }
    finished();
  }, [finished](){
    finished();
  });
}

void EntryRepository::store(const TextEntry& entry, const StoreSuccess& success, const StoreError& error)
{
  const auto prefix = QString::number(entry.timestamp.toUTC().toSecsSinceEpoch());
  const auto extension = fileFormat.extension();
  const auto location = generateFileLocation(prefix, extension);
  const auto filename = location + extension;

  const auto content = fileFormat.serialize(entry);

  filesystem.write(filename, content, [&success, location]{
    success(location);
  }, error);
}

QString EntryRepository::generateFileLocation(const QString &prefix, const QString &extension) const
{
  unsigned suffix = 0;
  auto location = prefix;

  while (filesystem.exists(location + extension)) {
    suffix++;
    location = prefix + "_" + QString::number(suffix);
  }

  return location;
}


}
