/*
 * (C) Copyright 2018 Urs Fässler
 *
 * SPDX-License-Identifier: AGPLv3
 */

#pragma once

#include "FileFormat.h"


namespace core
{


class XmlFileFormat :
    public FileFormat
{
  public:
    QString extension() const override;

    TextEntry parse(const QString&) const override;
    QString serialize(const TextEntry&) const override;
};


}
