/*
 * (C) Copyright 2018 Urs Fässler
 *
 * SPDX-License-Identifier: AGPLv3
 */

#pragma once

#include <QString>
#include <QDateTime>
#include <QStringList>


namespace core
{


struct TextEntry
{
    QStringList tags;
    QDateTime timestamp;
    QString content;
};

bool operator==(const TextEntry&, const TextEntry&);


}
