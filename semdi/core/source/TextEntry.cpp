/*
 * (C) Copyright 2018 Urs Fässler
 *
 * SPDX-License-Identifier: AGPLv3
 */

#include "TextEntry.h"

namespace core
{


bool operator==(const TextEntry& left, const TextEntry& right)
{
  return
      (left.timestamp == right.timestamp) &&
      (left.tags == right.tags) &&
      (left.content == right.content);
}


}
