/*
 * (C) Copyright 2018 Urs Fässler
 *
 * SPDX-License-Identifier: AGPLv3
 */

#include "EntryRepository.h"
#include "adapter/InMemoryFilesystem.h"
#include "XmlFileFormat.h"
#include "../../viewmodel/feature-tests/cute-gtest.h" //TODO move to a common place
#include <gtest/gtest.h>


namespace
{

using namespace testing;


class EntryRepositoryTest :
    public Test
{
public:
    adapter::InMemoryFilesystem filesystem{};
    core::XmlFileFormat fileFormat{};
    core::EntryRepository testee{filesystem, fileFormat};
};


TEST_F(EntryRepositoryTest, use_different_filename_when_preferred_already_exists)
{
  const QStringList expected{"0.xml", "0_1.xml"};
  filesystem.write("0.xml", "", [](){}, [](){});

  testee.store({{}, QDateTime::fromSecsSinceEpoch(0), {}}, [](const QString&){}, [](){});

  ASSERT_EQ(expected, filesystem.list());
}

TEST_F(EntryRepositoryTest, use_different_filename_when_the_2_preferred_already_exists)
{
  const QStringList expected{"0.xml", "0_1.xml", "0_2.xml"};
  filesystem.write("0.xml", "", [](){}, [](){});
  filesystem.write("0_1.xml", "", [](){}, [](){});

  testee.store({{}, QDateTime::fromSecsSinceEpoch(0), {}}, [](const QString&){}, [](){});

  ASSERT_EQ(expected, filesystem.list());
}


}
