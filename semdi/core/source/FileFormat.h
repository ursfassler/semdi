/*
 * (C) Copyright 2018 Urs Fässler
 *
 * SPDX-License-Identifier: AGPLv3
 */

#pragma once

#include <QString>


namespace core
{

class TextEntry;


class FileFormat
{
  public:
    virtual QString extension() const = 0;

    virtual TextEntry parse(const QString&) const = 0;
    virtual QString serialize(const TextEntry&) const = 0;
};


}
