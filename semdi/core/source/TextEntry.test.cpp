/*
 * (C) Copyright 2018 Urs Fässler
 *
 * SPDX-License-Identifier: AGPLv3
 */

#include "TextEntry.h"
#include <gtest/gtest.h>


namespace
{

using namespace testing;


class TextEntryTest :
    public Test
{
public:
    const core::TextEntry entry1{{"a", "b"}, {{2011, 2, 3}, {13, 45, 12}}, "Hello World"};
    const core::TextEntry entry2{{"a", "b"}, {{2011, 2, 3}, {13, 45, 12}}, "Hello World"};
    const core::TextEntry entry3{{"c"}, {{1999, 2, 3}, {13, 45, 12}}, "Test"};
};


TEST_F(TextEntryTest, entry_is_equal_to_itself)
{
  ASSERT_TRUE(entry1 == entry1);
}

TEST_F(TextEntryTest, entry_is_equal_to_another_with_the_same_values)
{
  ASSERT_TRUE(entry1 == entry2);
}

TEST_F(TextEntryTest, entry_is_not_equal_to_another_with_different_values)
{
  ASSERT_FALSE(entry1 == entry3);
}

TEST_F(TextEntryTest, entries_are_equal_when_empty)
{
  const core::TextEntry left{};
  const core::TextEntry right{};

  ASSERT_TRUE(core::TextEntry{} == core::TextEntry{});
}

TEST_F(TextEntryTest, entries_are_not_equal_when_tags_are_different)
{
  const core::TextEntry left{{"a"}, {}, {}};
  const core::TextEntry right{{"a", "b"}, {}, {}};

  ASSERT_FALSE(left == right);
}

TEST_F(TextEntryTest, entries_are_not_equal_when_dates_are_different)
{
  const core::TextEntry left{{}, {{2011, 2, 3}, {13, 45, 12}}, {}};
  const core::TextEntry right{{}, {{1999, 2, 3}, {13, 45, 12}}, {}};

  ASSERT_FALSE(left == right);
}

TEST_F(TextEntryTest, entries_are_not_equal_when_contents_are_different)
{
  const core::TextEntry left{{}, {}, "Hello"};
  const core::TextEntry right{{}, {}, "World"};

  ASSERT_FALSE(left == right);
}


}
