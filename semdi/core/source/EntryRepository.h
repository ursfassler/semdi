/*
 * (C) Copyright 2018 Urs Fässler
 *
 * SPDX-License-Identifier: AGPLv3
 */

#pragma once

#include "TextEntry.h"
#include "Filesystem.h"
#include <QMap>
#include <QList>
#include <QString>
#include <QSet>


namespace core
{

class FileFormat;


class EntryRepository
{
  public:
    EntryRepository(Filesystem&, const FileFormat&);

    typedef std::function<bool(const TextEntry&)> Specification;
    typedef QList<TextEntry> GetReturnType;
    typedef std::function<void(const GetReturnType&)> GetSuccessHandler;
    void get(const Specification&, const GetSuccessHandler&);

    typedef std::function<void(const QString&)> StoreSuccess;
    typedef std::function<void()> StoreError;
    void store(const TextEntry&, const StoreSuccess&, const StoreError&);

  private:
    Filesystem& filesystem;
    const FileFormat& fileFormat;

    std::size_t filesToParse{};
    GetReturnType entries{};

    void loadFile(const Filesystem::Filename&, const Specification&, const std::function<void(void)>&);
    QString generateFileLocation(const QString& prefix, const QString& extension) const;
};


}
