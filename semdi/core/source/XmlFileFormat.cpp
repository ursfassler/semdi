/*
 * (C) Copyright 2018 Urs Fässler
 *
 * SPDX-License-Identifier: AGPLv3
 */

#include "XmlFileFormat.h"

#include "TextEntry.h"
#include <QDomDocument>
#include <QXmlStreamWriter>


namespace core
{
namespace
{

const QString RootName{"entry"};
const QString AttrTimestamp{"timestamp"};
const Qt::DateFormat TimestampFormat{Qt::ISODate};
const QString AttrTags{"tags"};
const QString TagsSeparator{" "};

}


QString XmlFileFormat::extension() const
{
  return ".xml";
}

TextEntry XmlFileFormat::parse(const QString& value) const
{
  QDomDocument doc;
  doc.setContent(value);

  //TODO check if elemnts are present
  const auto entry = doc.documentElement();
  const auto timestampText = entry.attribute(AttrTimestamp);
  const auto timestamp = QDateTime::fromString(timestampText, TimestampFormat);
  const auto textNode = entry.firstChild();
  const auto text = textNode.nodeValue().trimmed();
  const auto tags = entry.attribute(AttrTags).split(TagsSeparator, QString::SkipEmptyParts);
  return {tags, timestamp, text};
}

QString XmlFileFormat::serialize(const TextEntry& value) const
{
  // https://stackoverflow.com/questions/21976264/qt-isodate-formatted-date-time-including-timezone
  auto time = value.timestamp;
  int offset = time.offsetFromUtc();
  time.setOffsetFromUtc(offset);
  const auto timeString = time.toString(TimestampFormat);

  QString result{};

  QXmlStreamWriter stream{&result};
  stream.setAutoFormatting(true);
  stream.setAutoFormattingIndent(2);

  stream.writeStartDocument();
  stream.writeStartElement(RootName);

  stream.writeAttribute(AttrTags, value.tags.join(TagsSeparator));
  stream.writeAttribute(AttrTimestamp, timeString);
  stream.writeCharacters(value.content);

  stream.writeEndElement();
  stream.writeEndDocument();

  return result;
}


}
