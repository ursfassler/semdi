/*
 * (C) Copyright 2018 Urs Fässler
 *
 * SPDX-License-Identifier: AGPLv3
 */

#pragma once

#include <QString>
#include <QStringList>
#include <functional>

namespace core
{


class Filesystem
{
public:
    virtual ~Filesystem() = default;

    typedef QString Filename;
    typedef QString Content;
    typedef QStringList DirectoryList;
    typedef std::function<void()> WriteSuccessHandler;
    typedef std::function<void()> WriteErrorHandler;
    typedef std::function<void(const Content&)> ReadSuccessHandler;
    typedef std::function<void()> ReadErrorHandler;
    typedef std::function<void(const DirectoryList&)> ListSuccessHandler;

    virtual void write(const Filename&, const Content&, const WriteSuccessHandler&, const WriteErrorHandler&) = 0;
    virtual void read(const Filename&, const ReadSuccessHandler&, const ReadErrorHandler&) const = 0;
    virtual void list(const ListSuccessHandler&) const = 0;
    virtual bool exists(const Filename&) const = 0;

};


}
