import QtQuick 2.9
import QtQuick.Controls 1.6
import QtQuick.Layouts 1.11

ColumnLayout {
    property var model: []
    signal selected(var entry)

    spacing: anchors.margins

    RowLayout {
        Layout.fillWidth: true

        TextInput {
            Layout.alignment: Qt.AlignVCenter
            Layout.fillWidth: true

            text: model.filter
            onTextChanged: {
                model.filter = text
            }
        }

        Button {
            Layout.alignment: Qt.AlignVCenter

            text: "Search"
            onClicked: model.update()
        }
    }

    ListView {
        Layout.fillWidth: true
        Layout.fillHeight: true

        spacing: 10
        clip: true
        model: parent.model

        delegate: Rectangle {
            width: parent.width
            height: childrenRect.height
            radius: 2

            ColumnLayout {
                anchors.top: parent.top
                anchors.left: parent.left
                anchors.right: parent.right
                anchors.margins: spacing

                spacing: parent.radius

                Text {
                    Layout.fillWidth: true
                    wrapMode: Text.Wrap
                    text: model.timestamp.toLocaleString()
                }

                Tags {
                    Layout.fillWidth: true
                    spacing: parent.spacing
                    tagColor: "lightgray"
                    tags: model.tags
                }

                Text {
                    Layout.fillWidth: true
                    wrapMode: Text.Wrap
                    text: content
                }
            }

            MouseArea {
                anchors.fill: parent
                onClicked: selected(model)
            }
        }
    }
}
