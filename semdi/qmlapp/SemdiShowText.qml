import QtQuick 2.9
import QtQuick.Controls 1.6
import QtQuick.Layouts 1.11

ColumnLayout {
    property var model: []

    anchors.margins: 10
    spacing: anchors.margins

    id: root

    Label {
        text: model.timestamp
    }

    Tags {
        Layout.fillWidth: true
        spacing: root.spacing
        tags: model.tags
    }

    Text {
        Layout.fillHeight: true
        Layout.fillWidth: true
        wrapMode: Text.Wrap

        text: model.content
    }
}
