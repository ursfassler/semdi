#include "viewmodel/source/AddText.h"
#include "viewmodel/source/Entries.h"
#include "services/source/Search.h"
#include "core/source/EntryRepository.h"
#include "core/source/XmlFileFormat.h"
#include "desktop/QtFilesystem.h"
#include <cute-adapter-production/Time.h>
#include <QGuiApplication>
#include <QQmlApplicationEngine>
#include <QQmlContext>

int main(int argc, char *argv[])
{
  QGuiApplication app(argc, argv);
  app.setApplicationName("semdi");

  QQmlApplicationEngine engine;

  adapter::QtFilesystem filesystem{};
  cute_adapter_production::Time time{};
  const core::XmlFileFormat fileFormat{};

  core::EntryRepository repository{filesystem, fileFormat};

  viewmodel::AddText addTextModel{repository, time};
  engine.rootContext()->setContextProperty("addTextModel", &addTextModel);

  service::Search searchService{repository};
  viewmodel::Entries entriesModel{searchService};
  engine.rootContext()->setContextProperty("entriesModel", &entriesModel);

  engine.load(QUrl(QStringLiteral("qrc:/main.qml")));
  if (engine.rootObjects().isEmpty()) {
    return -1;
  }

  return app.exec();
}
