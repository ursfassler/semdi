import QtQuick 2.9
import QtQuick.Controls 1.6
import QtQuick.Layouts 1.11

ApplicationWindow {
    visible: true
    title: qsTr("semdi")

    width: 400
    height: 400

    ColumnLayout {
        anchors.fill: parent
        anchors.margins: 10
        spacing: anchors.margins

        Button {
            text: "back"
            onClicked: stackView.pop()
            enabled: stackView.depth > 1
        }

        StackView {
            Layout.fillHeight: true
            Layout.fillWidth: true

            id: stackView

            initialItem: landing
        }
    }

    Component {
        id: landing
        ColumnLayout {
            Button {
                text: "add"
                onClicked: stackView.push({item: Qt.resolvedUrl("SemdiAddText.qml"), properties: {model: addTextModel}});
            }
            Button {
                text: "search"
                onClicked: stackView.push(search)
            }
        }
    }

    Component {
        id: search
        SemdiEntries {
            model: entriesModel

            onSelected: {
                stackView.push({item: Qt.resolvedUrl("SemdiShowText.qml"), properties: {model: entry}});
            }
        }
    }

    statusBar: StatusBar {
        RowLayout {
            anchors.fill: parent

            Item {
                Layout.fillWidth: true
            }

            Label {
                horizontalAlignment: Text.AlignRight

                text: addTextModel.message
            }
        }
    }
}
