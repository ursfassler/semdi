import QtQuick 2.0

Flow {
    property var tags: []
    property color tagColor: "white"

    id: root

    Repeater {
        model: tags

        delegate: Rectangle {
            property string text: modelData

            implicitWidth: childrenRect.width + root.spacing
            implicitHeight: childrenRect.height + root.spacing

            radius: root.spacing / 2
            color: tagColor

            Text {
                x: root.spacing / 2
                y: root.spacing / 2
                text: modelData
            }
        }
    }
}
