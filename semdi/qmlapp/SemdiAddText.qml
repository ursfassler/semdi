import QtQuick 2.9
import QtQuick.Controls 1.6
import QtQuick.Layouts 1.11

ColumnLayout {
    property var model: []

    spacing: anchors.margins

    Label {
        text: model.timestamp
        MouseArea {
            anchors.fill: parent
            onClicked: model.updateTimestamp()
        }
    }

    TextInput {
        Layout.fillWidth: true

        text: model.tags
        onTextChanged: {
            model.tags = text
        }
    }

    TextArea {
        Layout.fillHeight: true
        Layout.fillWidth: true

        text: model.content
        onTextChanged: {
            model.content = text
        }
    }

    Button {
        text: qsTr("Add")

        onClicked: model.commit()
    }
}
