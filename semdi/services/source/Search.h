/*
 * (C) Copyright 2018 Urs Fässler
 *
 * SPDX-License-Identifier: AGPLv3
 */

#pragma once

#include "core/source/TextEntry.h"
#include <QList>
#include <functional>


namespace core
{

class EntryRepository;

}

namespace service
{


class Search
{
  public:
    Search(core::EntryRepository&);

    typedef QList<core::TextEntry> ReturnType;
    typedef std::function<void(const ReturnType&)> Success;
    void byTextMatch(const QString&, const Success&); //TODO make const

  private:
    core::EntryRepository& repository; //TODO make const

};


}
