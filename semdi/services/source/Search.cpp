/*
 * (C) Copyright 2018 Urs Fässler
 *
 * SPDX-License-Identifier: AGPLv3
 */

#include "Search.h"

#include "core/source/EntryRepository.h"

namespace service
{
namespace
{

bool newestFirst(const core::TextEntry& left, const core::TextEntry& right)
{
  return left.timestamp > right.timestamp;
}

bool contains(const QString& content, const QString& substring)
{
  return content.contains(substring, Qt::CaseInsensitive);
}

core::EntryRepository::Specification textMatchSpecification(const QString& text)
{
  return [text](const core::TextEntry& entry){
    const auto inContent = contains(entry.content, text);
    const auto inTags = std::any_of(entry.tags.cbegin(), entry.tags.cend(), [text](const QString& tag){
      return contains(tag, text);
    });
    return inContent || inTags;
  };
}

}


Search::Search(core::EntryRepository& repository_) :
  repository{repository_}
{
}

void Search::byTextMatch(const QString& text, const Search::Success& success)
{
  const core::EntryRepository::Specification specification{textMatchSpecification(text)};

  repository.get(specification, [this, success](const core::EntryRepository::GetReturnType& value){
    ReturnType result{value};
    std::sort(result.begin(), result.end(), newestFirst);
    success(result);
  });
}


}
