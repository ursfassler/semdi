/*
 * (C) Copyright 2018 Urs Fässler
 *
 * SPDX-License-Identifier: AGPLv3
 */

#include "AddText.h"

#include "core/source/Filesystem.h"
#include "core/source/EntryRepository.h"
#include <cute-adapter/Time.h>
#include <QObject>
#include <QVariant>


namespace viewmodel
{


void AddText::commit()
{
  repo.store(entry, [this](const QString& value){
    resetEntry();
    setMessage("saved as " + value);
  }, [this]{
    setMessage("save failed");
  });
}

void AddText::resetEntry()
{
  entry = {};
  updateTimestamp();
  tagsChanged();
  contentChanged();
}

void AddText::reset()
{
  resetEntry();
  setMessage({});
}

void AddText::updateTimestamp()
{
  setTimestamp(time.now());
}

QString AddText::getTags() const
{
  return entry.tags.join(" ");
}

void AddText::setTags(const QString& value)
{
  const auto values = value.split(" ", QString::SkipEmptyParts);
  if (entry.tags != values) {
    entry.tags = values;
    tagsChanged();
    setMessage({});
  }
}

QString AddText::getContent() const
{
  return entry.content;
}

void AddText::setContent(const QString& value)
{
  if (entry.content != value) {
    entry.content = value;
    contentChanged();
    setMessage({});
  }
}

QDateTime AddText::getTimestamp() const
{
  return entry.timestamp;
}

void AddText::setTimestamp(const QDateTime& value)
{
  if (entry.timestamp != value) {
    entry.timestamp = value;
    timestampChanged();
    setMessage({});
  }
}

QString AddText::getMessage() const
{
  return message;
}

void AddText::setMessage(const QString& value)
{
  if (message != value) {
    message = value;
    messageChanged();
  }
}

AddText::AddText(core::EntryRepository& repo_, const cute_adapter::Time& time_) :
  repo{repo_},
  time{time_}
{
  reset();
}


}
