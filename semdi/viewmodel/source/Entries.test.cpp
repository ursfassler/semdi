/*
 * (C) Copyright 2018 Urs Fässler
 *
 * SPDX-License-Identifier: AGPLv3
 */

#include "Entries.h"
#include "core/adapter/InMemoryFilesystem.h"
#include "core/source/EntryRepository.h"
#include "core/source/XmlFileFormat.h"
#include "services/source/Search.h"
#include <gtest/gtest.h>
#include <QSignalSpy>
#include <QDateTime>
#include <QDate>
#include <QTime>


namespace
{

using namespace testing;


class EntriesTest :
    public Test
{
public:
    adapter::InMemoryFilesystem filesystem{};
    const core::XmlFileFormat fileFormat{};
    core::EntryRepository repository{filesystem, fileFormat};
    service::Search search{repository};
    viewmodel::Entries testee{search};

    QSignalSpy spyRowsAboutToBeInserted{&testee, &viewmodel::Entries::rowsAboutToBeInserted};
    QSignalSpy spyRowsAboutToBeRemoved{&testee, &viewmodel::Entries::rowsAboutToBeRemoved};

    void add(const std::vector<core::TextEntry>& value)
    {
      for (const auto& item : value) {
        repository.store(item, [](const QUrl&){
        }, [](){
        });
      }

      testee.update();
    }

    core::TextEntry entry(unsigned value)
    {
      return {{}, QDateTime::fromSecsSinceEpoch(value, Qt::UTC), QString::number(value)};
    }
    const core::TextEntry e10{entry(10)};
    const core::TextEntry e15{entry(15)};
    const core::TextEntry e20{entry(20)};
    const core::TextEntry e25{entry(25)};
    const core::TextEntry e30{entry(30)};
    const core::TextEntry e40{entry(40)};
    const core::TextEntry e50{entry(50)};
    const core::TextEntry e60{entry(60)};
};


TEST_F(EntriesTest, has_a_role_for_the_timestamp)
{
  ASSERT_TRUE(testee.roleNames().values().contains("timestamp"));
}

TEST_F(EntriesTest, has_a_role_for_the_tags)
{
  ASSERT_TRUE(testee.roleNames().values().contains("tags"));
}

TEST_F(EntriesTest, has_a_role_for_the_content)
{
  ASSERT_TRUE(testee.roleNames().values().contains("content"));
}

TEST_F(EntriesTest, items_can_be_read_when_items_added)
{
  add({e30, e20, e10});

  ASSERT_EQ(3, testee.rowCount());
}

TEST_F(EntriesTest, returns_nothing_when_asked_for_invalid_index)
{
  add({e10});

  ASSERT_EQ(QVariant{}, testee.data(testee.index(12345), viewmodel::Entries::RoleTimestamp));
}

TEST_F(EntriesTest, returns_nothing_when_asked_for_invalid_role)
{
  add({e10});

  ASSERT_EQ(QVariant{}, testee.data(testee.index(0), -1));
}

TEST_F(EntriesTest, can_read_the_timestamp_when_added)
{
  const QDateTime expected{QDate{1999, 1, 2}, QTime{5, 45, 13}};
  add({{{}, expected, {}}});

  const auto actual = testee.data(testee.index(0), viewmodel::Entries::RoleTimestamp).toDateTime();

  ASSERT_EQ(expected, actual);
}

TEST_F(EntriesTest, can_read_the_tags_when_added)
{
  const QStringList expected{"1", "test"};
  add({{{expected, {}, {}}}});

  const auto actual = testee.data(testee.index(0), viewmodel::Entries::RoleTags).toStringList();

  ASSERT_EQ(expected, actual);
}

TEST_F(EntriesTest, can_read_the_content_when_added)
{
  const QString expected{"hello world"};
  add({{{}, {}, expected}});

  const auto actual = testee.data(testee.index(0), viewmodel::Entries::RoleContent).toString();

  ASSERT_EQ(expected, actual);
}

TEST_F(EntriesTest, notify_correct_range_when_items_added_to_empty_list)
{
  add({e30, e20, e10});

  ASSERT_EQ(1, spyRowsAboutToBeInserted.count());
  ASSERT_EQ(0, spyRowsAboutToBeInserted.at(0).at(1).toInt());
  ASSERT_EQ(2, spyRowsAboutToBeInserted.at(0).at(2).toInt());
}

TEST_F(EntriesTest, notify_correct_range_when_items_added_to_existing_list_with_disting_items)
{
  add({e30, e20, e10});
  spyRowsAboutToBeInserted.clear();
  filesystem.clear();

  add({e60, e50, e40});

  ASSERT_EQ(1, spyRowsAboutToBeInserted.count());
  ASSERT_EQ(0, spyRowsAboutToBeInserted.at(0).at(1).toInt());
  ASSERT_EQ(2, spyRowsAboutToBeInserted.at(0).at(2).toInt());
}

TEST_F(EntriesTest, notify_correct_range_when_adding_additional_items_to_end_of_existing_list)
{
  add({e50, e40, e30});
  spyRowsAboutToBeInserted.clear();

  add({e20, e10});

  ASSERT_EQ(1, spyRowsAboutToBeInserted.count());
  ASSERT_EQ(3, spyRowsAboutToBeInserted.at(0).at(1).toInt());
  ASSERT_EQ(4, spyRowsAboutToBeInserted.at(0).at(2).toInt());
}

TEST_F(EntriesTest, notify_correct_range_when_adding_additional_items_in_between_existing_list)
{
  add({e30, e20, e10});
  spyRowsAboutToBeInserted.clear();
  spyRowsAboutToBeRemoved.clear();

  add({e25, e15});

  ASSERT_EQ(1, spyRowsAboutToBeRemoved.count());
  ASSERT_EQ(0, spyRowsAboutToBeRemoved.at(0).at(1).toInt());
  ASSERT_EQ(2, spyRowsAboutToBeRemoved.at(0).at(2).toInt());

  ASSERT_EQ(1, spyRowsAboutToBeInserted.count());
  ASSERT_EQ(0, spyRowsAboutToBeInserted.at(0).at(1).toInt());
  ASSERT_EQ(4, spyRowsAboutToBeInserted.at(0).at(2).toInt());
}

TEST_F(EntriesTest, does_not_notify_when_same_items_are_returned)
{
  add({e30, e20, e10});
  spyRowsAboutToBeInserted.clear();
  spyRowsAboutToBeRemoved.clear();

  add({});

  ASSERT_EQ(0, spyRowsAboutToBeRemoved.count());
  ASSERT_EQ(0, spyRowsAboutToBeInserted.count());
}

TEST_F(EntriesTest, add_entries_with_the_same_content_as_an_existing_one)
{
  const core::TextEntry e100{{}, QDateTime::fromSecsSinceEpoch(100), "Test"};
  const core::TextEntry e200{{}, QDateTime::fromSecsSinceEpoch(200), "Test"};
  add({e200});
  spyRowsAboutToBeInserted.clear();
  spyRowsAboutToBeRemoved.clear();

  add({e100});

  ASSERT_EQ(0, spyRowsAboutToBeRemoved.count());
  ASSERT_EQ(1, spyRowsAboutToBeInserted.count());
}

TEST_F(EntriesTest, add_new_entries_with_the_same_content)
{
  const core::TextEntry e100{{}, QDateTime::fromSecsSinceEpoch(100), "Test"};
  const core::TextEntry e200{{}, QDateTime::fromSecsSinceEpoch(200), "Test"};

  add({e200, e100});

  ASSERT_EQ(0, spyRowsAboutToBeRemoved.count());
  ASSERT_EQ(1, spyRowsAboutToBeInserted.count());
}


}
