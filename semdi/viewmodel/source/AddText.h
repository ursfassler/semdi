/*
 * (C) Copyright 2018 Urs Fässler
 *
 * SPDX-License-Identifier: AGPLv3
 */

#pragma once

#include "core/source/TextEntry.h"
#include <QObject>
#include <QDateTime>


namespace cute_adapter
{

class Time;

}

namespace core
{

class EntryRepository;

}

namespace viewmodel
{


class AddText :
    public QObject
{
    Q_OBJECT

    Q_PROPERTY(QString tags READ getTags WRITE setTags NOTIFY tagsChanged)
    Q_PROPERTY(QString content READ getContent WRITE setContent NOTIFY contentChanged)
    Q_PROPERTY(QDateTime timestamp READ getTimestamp WRITE setTimestamp NOTIFY timestampChanged)
    Q_PROPERTY(QString message READ getMessage NOTIFY messageChanged)

  public:
    AddText(core::EntryRepository&, const cute_adapter::Time&);

    QString getTags() const;
    void setTags(const QString&);

    QString getContent() const;
    void setContent(const QString&);

    QDateTime getTimestamp() const;
    void setTimestamp(const QDateTime&);

    QString getMessage() const;

    Q_INVOKABLE void commit();
    void reset();
    Q_INVOKABLE void updateTimestamp();

  signals:
    void tagsChanged();
    void contentChanged();
    void timestampChanged();
    void messageChanged();

  private:
    core::EntryRepository& repo;
    const cute_adapter::Time& time;

    core::TextEntry entry{};
    void resetEntry();

    QString message{};
    void setMessage(const QString&);

};


}
