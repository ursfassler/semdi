/*
 * (C) Copyright 2018 Urs Fässler
 *
 * SPDX-License-Identifier: AGPLv3
 */

#pragma once

#include <core/source/TextEntry.h>
#include <QAbstractListModel>
#include <QList>
#include <QUrl>


namespace service
{

class Search;

}

namespace viewmodel
{


class Entries :
    public QAbstractListModel
{
    Q_OBJECT

    Q_PROPERTY(QString filter READ getFilter WRITE setFilter NOTIFY filterChanged)

  public:
    enum Role
    {
      RoleTimestamp = Qt::UserRole + 1,
      RoleTags,
      RoleContent,
    };

    Entries(service::Search&);

    QHash<int, QByteArray> roleNames() const override;

    int rowCount(const QModelIndex &parent = QModelIndex()) const override;
    QVariant data(const QModelIndex &index, int role = Qt::DisplayRole) const override;

    Q_INVOKABLE void update();

  signals:
    void filterChanged();

  private:
    service::Search& search;
    QList<core::TextEntry> cache{};

    QString filter{};
    QString getFilter() const;
    void setFilter(const QString&);

    void clear();
    void append(const QList<core::TextEntry>&);
};


}
