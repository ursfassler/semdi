/*
 * (C) Copyright 2018 Urs Fässler
 *
 * SPDX-License-Identifier: AGPLv3
 */

#include "Entries.h"

#include "services/source/Search.h"
#include <QDateTime>


namespace viewmodel
{


Entries::Entries(service::Search& search_) :
  search{search_}
{
}

QHash<int, QByteArray> Entries::roleNames() const
{
  QHash<int, QByteArray> roles;
  roles[RoleTimestamp] = "timestamp";
  roles[RoleTags] = "tags";
  roles[RoleContent] = "content";
  return roles;
}

int Entries::rowCount(const QModelIndex&) const
{
  return cache.size();
}

QVariant Entries::data(const QModelIndex &index, int role) const
{
  const auto validIndex = (0 <= index.row()) && (index.row() < cache.size());
  if (!validIndex) {
    return {};
  }

  const auto& entry = cache[index.row()];

  switch (role) {
    case RoleTimestamp:
      return entry.timestamp;

    case RoleTags:
      return entry.tags;

    case RoleContent:
      return entry.content;

    default:
      return {};
  }
}

void Entries::clear()
{
  beginRemoveRows(QModelIndex(), 0, cache.size()-1);
  cache.clear();
  endRemoveRows();
}

void Entries::append(const QList<core::TextEntry>& value)
{
  beginInsertRows(QModelIndex(), cache.size(), cache.size() + value.size()-1);
  cache.append(value);
  endInsertRows();
}

void Entries::update()
{
  search.byTextMatch(filter, [this](const service::Search::ReturnType& value){
    const auto isAppend = (value.size() >= cache.size()) && (value.mid(0, cache.size()) == cache);

    if (isAppend) {
      // expected default cases

      const auto hasChange = cache.size() != value.size();
      if (hasChange) {
        const auto tail = value.mid(cache.size());
        append(tail);
      }
    } else {
      // unoptimised case
      clear();
      append(value);
    }
  });
}

QString Entries::getFilter() const
{
  return filter;
}

void Entries::setFilter(const QString& value)
{
  if (value != filter) {
    filter = value;
    filterChanged();
  }
}


}
