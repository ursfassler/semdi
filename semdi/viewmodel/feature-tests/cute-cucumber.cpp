/*
 * (C) Copyright 2018 Urs Fässler
 *
 * SPDX-License-Identifier: AGPLv3
 */

#include "cute-cucumber.h"
#include "Converter.h"


void operator>>(std::istringstream& stream, QString& value)
{
  const std::string stdstr = stream.str();
  value = QString::fromStdString(stdstr);
}

void operator>>(std::istringstream& stream, QDateTime& value)
{
  const std::string stdstr = stream.str();
  const auto qstr = QString::fromStdString(stdstr);
  value = Converter::toDateTime(qstr);
}
