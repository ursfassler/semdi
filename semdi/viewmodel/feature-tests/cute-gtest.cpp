/*
 * (C) Copyright 2018 Urs Fässler
 *
 * SPDX-License-Identifier: AGPLv3
 */

#include "cute-gtest.h"


std::ostream& operator<<(std::ostream& stream, const QString& value)
{
  stream << value.toStdString();
  return stream;
}

void PrintTo(const QString& value, std::ostream* stream)
{
    *stream << value;
}
