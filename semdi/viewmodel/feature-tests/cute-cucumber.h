/*
 * (C) Copyright 2018 Urs Fässler
 *
 * SPDX-License-Identifier: AGPLv3
 */

#pragma once

#include <QString>
#include <QDateTime>
#include <sstream>


// for cucumber parser
void operator>>(std::istringstream&, QString&);
void operator>>(std::istringstream&, QDateTime&);
