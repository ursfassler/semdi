/*
 * (C) Copyright 2018 Urs Fässler
 *
 * SPDX-License-Identifier: AGPLv3
 */

#include <gtest/gtest.h>
#include <cucumber-cpp/autodetect.hpp>

#include "cute-cucumber.h"
#include "ViewModelContext.h"


namespace
{


GIVEN("^the UTC offset is (\\d+) hours$")
{
  REGEX_PARAM(unsigned, hours);

  cucumber::ScenarioScope<CuteAdapterTimeContext> context;
  context->time.setTimeSpec(Qt::OffsetFromUTC);
  context->time.setOffsetFromUtc(hours * 60 * 60);
}

GIVEN("^the time is \"([^\"]*)\"$")
{
  REGEX_PARAM(QDateTime, time);

  cucumber::ScenarioScope<CuteAdapterTimeContext> context;
  context->timeFactory.forward_time_to(time);
}


}
