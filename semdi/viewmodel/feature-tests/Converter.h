/*
 * (C) Copyright 2018 Urs Fässler
 *
 * SPDX-License-Identifier: AGPLv3
 */

#include <QVariant>
#include <QString>
#include <QAbstractListModel>
#include <vector>
#include <map>
#include <string>


class Converter
{
  public:
    static void setDateFormat(Qt::DateFormat);

    static QString toString(const QVariant&);
    static QDateTime toDateTime(const QString&);
    static std::vector<std::map<std::string, std::string> > toTable(const QAbstractListModel&);

  private:
    static Qt::DateFormat dateFormat;

};

