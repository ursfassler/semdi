/*
 * (C) Copyright 2018 Urs Fässler
 *
 * SPDX-License-Identifier: AGPLv3
 */

#include <gtest/gtest.h>
#include <cucumber-cpp/autodetect.hpp>

#include "cute-gtest.h"
#include "cute-cucumber.h"
#include "PropertyReflection.h"
#include "Converter.h"
#include <QVariant>
#include <QSignalSpy>
#include <QMetaType>
#include <QDateTime>


namespace
{


GIVEN("^I clear all change notifications$")
{
  cucumber::ScenarioScope<PropertyReflection> context;
  context->clearSpyInvocations();
}

WHEN("^I change the property ([^\\.]*)\\.([^\\.]*) to \"([^\"]*)\"$")
{
  REGEX_PARAM(QString, object);
  REGEX_PARAM(QString, property);
  REGEX_PARAM(QString, value);

  cucumber::ScenarioScope<PropertyReflection> context;
  context->setValue(object, property, value);
}

WHEN("^I change the property ([^\\.]*)\\.([^\\.]*) to:$")
{
  REGEX_PARAM(QString, object);
  REGEX_PARAM(QString, property);
  REGEX_PARAM(QString, value);

  cucumber::ScenarioScope<PropertyReflection> context;
  context->setValue(object, property, value);
}

THEN("^I expect that the property ([^\\.]*)\\.([^\\.]*) changed$")
{
  REGEX_PARAM(QString, object);
  REGEX_PARAM(QString, property);

  cucumber::ScenarioScope<PropertyReflection> context;
  const auto spy = context->getSpy(object, property);
  ASSERT_EQ(1, spy->count()) << "for property " << object << "." << property;
}

THEN("^I expect that the property ([^\\.]*)\\.([^\\.]*) did not change$")
{
  REGEX_PARAM(QString, object);
  REGEX_PARAM(QString, property);

  cucumber::ScenarioScope<PropertyReflection> context;
  const auto spy = context->getSpy(object, property);
  ASSERT_EQ(0, spy->count());
}

THEN("^I expect the property ([^\\.]*)\\.([^\\.]*) to be:$")
{
  REGEX_PARAM(QString, object);
  REGEX_PARAM(QString, property);
  REGEX_PARAM(QString, expected);

  cucumber::ScenarioScope<PropertyReflection> context;
  const auto actual = Converter::toString(context->getValue(object, property));
  ASSERT_EQ(expected, actual);
}

THEN("^I expect the property ([^\\.]*)\\.([^\\.]*) to be \"([^\"]*)\"$")
{
  REGEX_PARAM(QString, object);
  REGEX_PARAM(QString, property);
  REGEX_PARAM(QString, expected);

  cucumber::ScenarioScope<PropertyReflection> context;
  const auto actual = Converter::toString(context->getValue(object, property));
  ASSERT_EQ(expected, actual);
}


}
