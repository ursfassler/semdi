/*
 * (C) Copyright 2018 Urs Fässler
 *
 * SPDX-License-Identifier: AGPLv3
 */

#pragma once

#include <QSignalSpy>


class PropertyReflection
{
  public:
    void reflect(QObject *object);

    QVariant getValue(const QString& object, const QString& property) const;
    void setValue(const QString& object, const QString& property, const QString& value);

    QSignalSpy* getSpy(const QString& object, const QString& property) const;
    void clearSpyInvocations();

  private:
    QMap<QString, QObject*> objects{};
    QMap<QPair<QString, QString>, QSignalSpy*> spys{};
};
