/*
 * (C) Copyright 2018 Urs Fässler
 *
 * SPDX-License-Identifier: AGPLv3
 */

#pragma once

#include <ostream>
#include <QString>


// for gtest additional messages
std::ostream& operator<<(std::ostream&, const QString&);

// for gtest assertions
void PrintTo(const QString&, std::ostream*);
