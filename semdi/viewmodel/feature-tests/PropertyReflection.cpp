/*
 * (C) Copyright 2018 Urs Fässler
 *
 * SPDX-License-Identifier: AGPLv3
 */

#include "PropertyReflection.h"


void PropertyReflection::reflect(QObject *object)
{
  objects[object->objectName()] = object;

  const QMetaObject* metaObject = object->metaObject();
  for (int i = metaObject->propertyOffset(); i < metaObject->propertyCount(); ++i) {
    const QMetaProperty property = metaObject->property(i);

    const QString name = "2" + property.notifySignal().name() + "()";
    QSignalSpy* const spy = new QSignalSpy(object, name.toStdString().c_str());

    const QString propertyName = property.name();
    const QPair<QString, QString> pname = {object->objectName(), propertyName};

    spys[pname] = spy;
  }
}

QVariant PropertyReflection::getValue(const QString &object, const QString &property) const
{
  QObject* obj = objects.value(object);

  if (!obj) {
    throw std::invalid_argument("object " + object.toStdString() + "not found");
  }

  return obj->property(property.toStdString().c_str());
}

void PropertyReflection::setValue(const QString &object, const QString &property, const QString &value)
{
  QObject* obj = objects.value(object);

  if (!obj) {
    throw std::invalid_argument("object " + object.toStdString() + " not found");
  }

  obj->setProperty(property.toStdString().c_str(), value);
}

QSignalSpy *PropertyReflection::getSpy(const QString &object, const QString &property) const
{
  QSignalSpy* spy = spys.value({object, property});

  if (!spy || !spy->isValid()) {
    throw std::invalid_argument("no spy for property " + object.toStdString() + "." + property.toStdString());
  }

  return spy;
}

void PropertyReflection::clearSpyInvocations()
{
  for (auto& spy : spys) {
    spy->clear();
  }
}
