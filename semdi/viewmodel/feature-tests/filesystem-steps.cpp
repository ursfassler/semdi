/*
 * (C) Copyright 2018 Urs Fässler
 *
 * SPDX-License-Identifier: AGPLv3
 */

#include <gtest/gtest.h>
#include <cucumber-cpp/autodetect.hpp>
#include "feature-tests/cute-cucumber.h"
#include "core/adapter/InMemoryFilesystem.h"
#include "feature-tests/cute-gtest.h"

namespace
{

using cucumber::ScenarioScope;

GIVEN("^no new files can be created$")
{
  ScenarioScope<adapter::InMemoryFilesystem> filesystem;
  filesystem->setReadonly();
}

GIVEN("^I have the file \"([^\"]*)\" in the filesystem:$")
{
  REGEX_PARAM(core::Filesystem::Filename, filename);
  REGEX_PARAM(core::Filesystem::Content, content);

  ScenarioScope<adapter::InMemoryFilesystem> filesystem;
  filesystem->write(filename, content, []{
    ASSERT_TRUE(true);
  }, []{
    ASSERT_TRUE(false);
  });
}

THEN("^I expect to see the file \"([^\"]*)\" in the filesystem:$")
{
  REGEX_PARAM(core::Filesystem::Filename, filename);
  REGEX_PARAM(core::Filesystem::Content, content);

    ScenarioScope<adapter::InMemoryFilesystem> filesystem;
    filesystem->read(filename, [content](const core::Filesystem::Content& value){
      ASSERT_EQ(content, value);
    }, []{
      ASSERT_TRUE(false);
    });
}

THEN("^I expect no files in the filesystem$")
{
  ScenarioScope<adapter::InMemoryFilesystem> filesystem;
  ASSERT_EQ(core::Filesystem::DirectoryList{}, filesystem->list());
}

THEN("^I expect to see the following files in the filesystem:$")
{
  TABLE_PARAM(filesParam);

  core::Filesystem::DirectoryList expected{};
  for (const auto &entry : filesParam.hashes()) {
    expected.append(QString::fromStdString(entry.at("filename")));
  }
  expected.sort();

  ScenarioScope<adapter::InMemoryFilesystem> filesystem;
  core::Filesystem::DirectoryList actual = filesystem->list();
  actual.sort();

  ASSERT_EQ(expected, actual);
}


}
