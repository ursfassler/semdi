/*
 * (C) Copyright 2018 Urs Fässler
 *
 * SPDX-License-Identifier: AGPLv3
 */

#include "Converter.h"

#include <QDateTime>

Qt::DateFormat Converter::dateFormat = Qt::TextDate;

void Converter::setDateFormat(Qt::DateFormat value)
{
  dateFormat = value;
}

QString Converter::toString(const QVariant& value)
{
  switch (value.type()) {
    case QVariant::DateTime:
      return value.toDateTime().toString(dateFormat);

    case QVariant::StringList:
      return value.toStringList().join(" ");

    default:
      return value.toString();
  }
}

QDateTime Converter::toDateTime(const QString& value)
{
  return QDateTime::fromString(value, dateFormat);
}

std::vector<std::map<std::string,std::string>> Converter::toTable(const QAbstractListModel& value)
{
  std::vector<std::map<std::string,std::string>> result{};

  const auto roleNames = value.roleNames();
  for (int i = 0; i < value.rowCount(); i++) {
    std::map<std::string,std::string> map{};
    for (const auto& role : roleNames.keys()) {
      const auto data = value.data(value.index(i), role);
      map[roleNames[role].toStdString()] = toString(data).toStdString();
    }
    result.push_back(map);
  }

  return result;
}
