/*
 * (C) Copyright 2018 Urs Fässler
 *
 * SPDX-License-Identifier: AGPLv3
 */

#include <gtest/gtest.h>
#include <cucumber-cpp/autodetect.hpp>


#include "ViewModelContext.h"
#include "core/source/EntryRepository.h"
#include "viewmodel/source/Entries.h"
#include "source/AddText.h"
#include "Converter.h"
#include <cute-adapter-testing/SimulatedTimerFactory.h>
#include <cute-adapter-testing/SimulatedTime.h>

#include <QVariant>
#include <QSignalSpy>


namespace
{

using cucumber::ScenarioScope;

BEFORE()
{
  Converter::setDateFormat(Qt::ISODate);
}

GIVEN("^the ViewModel is started$")
{
  ScenarioScope<ViewModelContext> context;
}

WHEN("^I reset the text view model$")
{
  ScenarioScope<ViewModelContext> context;
  context->addText.reset();
}

WHEN("^I commit the entry$")
{
  ScenarioScope<ViewModelContext> context;
  context->addText.commit();
}

WHEN("^I update the timestamp$")
{
  ScenarioScope<ViewModelContext> context;
  context->addText.updateTimestamp();
}

WHEN("^I update the entry view$")
{
  ScenarioScope<ViewModelContext> context;
  context->entries.update();
}

THEN("^I expect to see the following entries:$")
{
  TABLE_PARAM(entriesParam);
  const auto& expected = entriesParam.hashes();

  ScenarioScope<ViewModelContext> context;
  const auto actual = Converter::toTable(context->entries);

  ASSERT_EQ(expected, actual);
}


}
