/*
 * (C) Copyright 2018 Urs Fässler
 *
 * SPDX-License-Identifier: AGPLv3
 */

#pragma once

#include <cute-adapter-testing/SimulatedTimerFactory.h>
#include <cute-adapter-testing/SimulatedTime.h>


class CuteAdapterTimeContext
{
  public:
    cute_adapter_testing::SimulatedTimerFactory timeFactory{};
    cute_adapter_testing::SimulatedTime time{timeFactory};

};
