/*
 * (C) Copyright 2018 Urs Fässler
 *
 * SPDX-License-Identifier: AGPLv3
 */

#pragma once

#include "CuteAdapterTimeContext.h"
#include "PropertyReflection.h"
#include "core/adapter/InMemoryFilesystem.h"
#include "source/AddText.h"
#include "core/source/EntryRepository.h"
#include "source/Entries.h"
#include "core/source/XmlFileFormat.h"
#include "services/source/Search.h"
#include <cucumber-cpp/internal/ContextManager.hpp>


class ViewModelContext
{
  public:
    cucumber::ScenarioScope<adapter::InMemoryFilesystem> filesystem{};
    cucumber::ScenarioScope<CuteAdapterTimeContext> time{};
    const core::XmlFileFormat fileFormat{};
    core::EntryRepository repository{*filesystem, fileFormat};
    viewmodel::AddText addText{repository, time->time};
    service::Search search{repository};
    viewmodel::Entries entries{search};
    cucumber::ScenarioScope<PropertyReflection> spy{};

    ViewModelContext()
    {
      addText.setObjectName("addText");
      spy->reflect(&addText);
      entries.setObjectName("entries");
      spy->reflect(&entries);
    }

};


