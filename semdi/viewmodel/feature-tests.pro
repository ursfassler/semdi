TEMPLATE = app

CONFIG -= app_bundle

QT -= gui
QT += testlib

OTHER_FILES += \
    "features/add text.feature" \
    "features/list entries.feature" \
    "features/show text.feature" \

HEADERS += \
    ../core/adapter/InMemoryFilesystem.h \
    feature-tests/ViewModelContext.h \
    feature-tests/CuteAdapterTimeContext.h \
    feature-tests/PropertyReflection.h \
    feature-tests/Converter.h \
    feature-tests/cute-gtest.h \
    feature-tests/cute-cucumber.h \

SOURCES += \
    ../core/adapter/InMemoryFilesystem.cpp \
    feature-tests/filesystem-steps.cpp \
    feature-tests/viewmodel-steps.cpp \
    feature-tests/cute-adapter-time-steps.cpp \
    feature-tests/PropertyReflection.cpp \
    feature-tests/PropertyReflection-steps.cpp \
    feature-tests/Converter.cpp \
    feature-tests/cute-gtest.cpp \
    feature-tests/cute-cucumber.cpp \

LIBS += -lcute-adapter-testing -lcute-adapter

include(viewmodel.pri)
include(../cucumber.pri)
