
HEADERS += \
    $$PWD/../viewmodel/source/AddText.h \
    $$PWD/../viewmodel/source/Entries.h \

SOURCES += \
    $$PWD/../viewmodel/source/AddText.cpp \
    $$PWD/../viewmodel/source/Entries.cpp \

include(../services/services.pri)
include(../common.pri)
