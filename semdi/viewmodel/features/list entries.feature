# language: en

Feature: list existing entries
  As a user
  I want see my entries
  In order select and open one


Background:
  Given the ViewModel is started
  And I have the file "100.xml" in the filesystem:
    """
    <entry timestamp="2010-01-10T01:55:05+01:00" tags=":)">Hello World</entry>
    """
  And I have the file "9999.xml" in the filesystem:
    """
    <entry timestamp="2018-10-27T22:55:05+02:00">Abc ef</entry>
    """
  And I have the file "990990990.xml" in the filesystem:
    """
    <entry timestamp="2000-10-27T22:55:05+02:00" tags="hello world">Test</entry>
    """
  And I have the file "55.md" in the filesystem:
    """
    = Test =
    """


Scenario: List all entries ordered by timestamp

  When I update the entry view

  Then I expect to see the following entries:
    | timestamp                 | tags        | content     |
    | 2018-10-27T22:55:05+02:00 |             | Abc ef      |
    | 2010-01-10T01:55:05+01:00 | :)          | Hello World |
    | 2000-10-27T22:55:05+02:00 | hello world | Test        |


Scenario: Search in entries
  Given I change the property entries.filter to "hell"

  When I update the entry view

  Then I expect to see the following entries:
    | timestamp                 | tags        | content     |
    | 2010-01-10T01:55:05+01:00 | :)          | Hello World |
    | 2000-10-27T22:55:05+02:00 | hello world | Test        |
