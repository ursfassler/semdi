# language: en

Feature: add a new text entry
  As a user
  I want to add a new text entry
  In order to save a thought or experience


Background:
  Given the UTC offset is 3 hours
  And the time is "2020-01-01T10:11:12+03:00"
  And the ViewModel is started


Scenario: Reset the viewmodel
  Given I clear all change notifications

  When I reset the text view model

  Then I expect that the property addText.timestamp changed
  And I expect the property addText.timestamp to be "2020-01-01T10:11:12+03:00"
  And I expect that the property addText.tags changed
  And I expect the property addText.tags to be ""
  And I expect that the property addText.content changed
  And I expect the property addText.content to be:
    """
    """
  And I expect the property addText.message to be ""


Scenario: Add a new text entry
  Given I change the property addText.timestamp to "2018-10-27T22:55:05+02:00"
  And I change the property addText.content to:
    """
    Hello World
    """
  And I change the property addText.tags to ":|"
  And I clear all change notifications

  When I commit the entry

  Then I expect that the property addText.timestamp changed
  And I expect the property addText.timestamp to be "2020-01-01T10:11:12+03:00"
  And I expect that the property addText.content changed
  And I expect the property addText.content to be:
    """
    """
  And I expect that the property addText.tags changed
  And I expect the property addText.tags to be ""
  And I expect that the property addText.message changed
  And I expect the property addText.message to be "saved as 1540673705"
  And I expect to see the file "1540673705.xml" in the filesystem:
    """
    <?xml version="1.0"?>
    <entry tags=":|" timestamp="2018-10-27T22:55:05+02:00">Hello World</entry>

    """


Scenario: Adding the new text entry fails
  Given no new files can be created
  And I change the property addText.timestamp to "2018-10-27T22:55:05+02:00"
  And I change the property addText.tags to "lala"
  And I change the property addText.content to:
    """
    Hello World
    """
  And I clear all change notifications

  When I commit the entry

  Then I expect that the property addText.timestamp did not change
  And I expect the property addText.timestamp to be "2018-10-27T22:55:05+02:00"
  And I expect that the property addText.tags did not change
  And I expect the property addText.tags to be "lala"
  And I expect that the property addText.content did not change
  And I expect the property addText.content to be:
    """
    Hello World
    """
  Then I expect that the property addText.message changed
  And I expect the property addText.message to be "save failed"


Scenario: Editing after adding text entry
  Given I commit the entry
  And I clear all change notifications

  When I change the property addText.content to:
    """
    Hello World
    """

  Then I expect that the property addText.message changed
  And I expect the property addText.message to be ""


Scenario: Update the timestamp
  Given I change the property addText.timestamp to "2018-10-27T22:55:05+02:00"
  And I change the property addText.content to:
    """
    Hello World
    """
  And I change the property addText.tags to ":|"
  And I clear all change notifications

  When I update the timestamp

  Then I expect that the property addText.timestamp changed
  And I expect the property addText.timestamp to be "2020-01-01T10:11:12+03:00"
  And I expect that the property addText.tags did not change
  And I expect that the property addText.content did not change


Scenario: Use the timestamp as filename
  Given I change the property addText.timestamp to "1970-01-01T00:00:42+00:00"

  When I commit the entry

  Then I expect to see the following files in the filesystem:
    | filename |
    | 42.xml   |


Scenario: Can add multiple entries with the same timestamp
  Given I change the property addText.content to:
    """
    A
    """
  And I commit the entry
  And I change the property addText.content to:
    """
    B
    """
  And I commit the entry
  And I change the property addText.content to:
    """
    C
    """
  And I commit the entry

  When I update the entry view

  Then I expect to see the following entries:
    | timestamp                 | tags | content |
    | 2020-01-01T10:11:12+03:00 |      | A       |
    | 2020-01-01T10:11:12+03:00 |      | B       |
    | 2020-01-01T10:11:12+03:00 |      | C       |
