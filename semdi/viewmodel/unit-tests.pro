TEMPLATE = app

CONFIG -= app_bundle

QT -= gui
QT += testlib

HEADERS += \
    source/Entries.h \
    $$PWD/../core/adapter/InMemoryFilesystem.h \

SOURCES += \
    source/Entries.cpp \
    source/Entries.test.cpp \
    $$PWD/../core/adapter/InMemoryFilesystem.cpp \

LIBS += -lcute-adapter-testing -lcute-adapter

include(../gtest.pri)
include(../common.pri)
include(../services/services.pri)
