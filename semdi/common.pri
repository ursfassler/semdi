
INCLUDEPATH += $$PWD/

OBJECTS_DIR = ./$$TARGET-obj/  # workaround for: https://bugreports.qt.io/browse/QTBUG-50803

CONFIG += warn_on
CONFIG += object_parallel_to_source
CONFIG += c++11

QMAKE_CXXFLAGS += -Wall -Wextra -pedantic
