/*
 * (C) Copyright 2018 Urs Fässler
 *
 * SPDX-License-Identifier: AGPLv3
 */

#include "QtFilesystem.h"

#include <QStandardPaths>
#include <QDebug>


namespace adapter
{


QtFilesystem::QtFilesystem() :
  root{QStandardPaths::writableLocation(QStandardPaths::AppLocalDataLocation)}
{
  qInfo() << "use data directory at" << root.absolutePath();
}

void QtFilesystem::write(const Filename& filename, const Content& content, const WriteSuccessHandler& success, const WriteErrorHandler& error)
{
  if (!root.mkpath(".")) {
    error();
    return;
  }

  QFile file{root.absoluteFilePath(filename)};
  if (!file.open(QIODevice::WriteOnly | QIODevice::Truncate)) {
    error();
    return;
  }
  const auto data = content.toUtf8();
  //TODO add saver method
  if (file.write(data) != data.size()) {
    error();
    return;
  }
  file.close();

  success();
}

void QtFilesystem::read(const Filename& filename, const ReadSuccessHandler& success, const ReadErrorHandler& error) const
{
  QFile file{root.absoluteFilePath(filename)};
  if (!file.open(QIODevice::ReadOnly)) {
    error();
    return;
  }

  //TODO add saver method
  const auto content = file.readAll();
  file.close();

  success(content);
}

void QtFilesystem::list(const Filesystem::ListSuccessHandler& success) const
{
  const auto files = root.entryList(QDir::Files);
  success(files);
}

bool QtFilesystem::exists(const core::Filesystem::Filename& value) const
{
  return root.exists(value);
}


}
