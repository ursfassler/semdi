/*
 * (C) Copyright 2018 Urs Fässler
 *
 * SPDX-License-Identifier: AGPLv3
 */

#pragma once

#include "core/source/Filesystem.h"
#include <QDir>

namespace adapter
{


class QtFilesystem :
        public core::Filesystem
{
public:
    QtFilesystem();

    void write(const Filename&, const Content&, const WriteSuccessHandler&, const WriteErrorHandler&) override;
    void read(const Filename&, const ReadSuccessHandler&, const ReadErrorHandler&) const override;
    void list(const ListSuccessHandler&) const override;
    bool exists(const Filename&) const override;

private:
    const QDir root;

};


}
