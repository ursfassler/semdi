/*
 * (C) Copyright 2018 Urs Fässler
 *
 * SPDX-License-Identifier: AGPLv3
 */

#include "mainwindow.h"

#include "ui_mainwindow.h"
#include "viewmodel/source/AddText.h"


MainWindow::MainWindow(viewmodel::AddText& addText_, QWidget* parent) :
    QMainWindow{parent},
    addText{addText_},
    ui{new Ui::MainWindow}
{
    ui->setupUi(this);

    connect(ui->pushButton, &QPushButton::clicked, &addText, &viewmodel::AddText::commit);

    connect(ui->plainTextEdit, &QPlainTextEdit::textChanged, [this]{
      addText.setContent(ui->plainTextEdit->toPlainText());
    });
    connect(ui->dateTimeEdit, &QDateTimeEdit::dateTimeChanged, [this]{
      addText.setTimestamp(ui->dateTimeEdit->dateTime());
    });
    connect(ui->lineEdit, &QLineEdit::textChanged, [this]{
      addText.setTags(ui->lineEdit->text());
    });

    connect(&addText, &viewmodel::AddText::contentChanged, [this]{
      const auto viewText = ui->plainTextEdit->toPlainText();
      const auto modelText = addText.getContent();
      if (viewText != modelText) {
        ui->plainTextEdit->setPlainText(modelText);
        ui->plainTextEdit->moveCursor(QTextCursor::End);
      }
    });
    connect(&addText, &viewmodel::AddText::tagsChanged, [this]{
      const auto viewText = ui->lineEdit->text();
      const auto modelText = addText.getTags();
      if (viewText != modelText) {
        ui->lineEdit->setText(modelText);
      }
    });
    connect(&addText, &viewmodel::AddText::timestampChanged, [this]{
      ui->dateTimeEdit->setDateTime(addText.getTimestamp());
    });
    connect(&addText, &viewmodel::AddText::messageChanged, [this]{
      ui->statusBar->showMessage(addText.getMessage());
    });

    addText.reset();
}

MainWindow::~MainWindow()
{
  delete ui;
}
