/*
 * (C) Copyright 2018 Urs Fässler
 *
 * SPDX-License-Identifier: AGPLv3
 */

#pragma once

#include <QMainWindow>
#include <functional>


namespace Ui
{

class MainWindow;

}

namespace viewmodel
{

class AddText;

}


class MainWindow :
    public QMainWindow
{
    Q_OBJECT

public:
    explicit MainWindow(viewmodel::AddText&, QWidget *parent = 0);
    ~MainWindow();

private:
    viewmodel::AddText &addText;
    Ui::MainWindow *ui;
};
