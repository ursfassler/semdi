/*
 * (C) Copyright 2018 Urs Fässler
 *
 * SPDX-License-Identifier: AGPLv3
 */

#include "mainwindow.h"
#include "core/source/EntryRepository.h"
#include "core/source/XmlFileFormat.h"
#include "viewmodel/source/AddText.h"
#include "QtFilesystem.h"
#include <cute-adapter-production/Time.h>
#include <QApplication>


int main(int argc, char *argv[])
{
    QApplication a(argc, argv);
    a.setApplicationName("semdi");

    adapter::QtFilesystem filesystem{};
    const core::XmlFileFormat fileFormat{};
    core::EntryRepository repository{filesystem, fileFormat};

    cute_adapter_production::Time cuteTime{};
    viewmodel::AddText addTextModel{repository, cuteTime};
    MainWindow w{addTextModel};

    w.show();

    return a.exec();
}
